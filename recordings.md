---
title: "recordings"
order: 6
in_menu: true
---
<p style="text-align: center;"><strong>Tombstones</strong></p>

<a href="https://elsewheremusic.bandcamp.com/album/michael-pisaro-liu-tombstones" style="display: block; text-align: center; margin: 0 auto;">
  <img src="images/COV_tombstones_circle.jpg" alt="Description de l'image" width="500" height="auto" style="display: block; margin: 0 auto;">
</a>

<p><strong>Yoann Bellefont</strong> Double Bass, <strong>Barbara Dang</strong> Direction, Piano, Voice, <strong>Ivann Cruz</strong> Acoustic Guitar, <strong>Raphaël Godeau</strong> Acoustic Guitar, <strong>Peter Orins</strong> Glockenspiel, Stones, Recording, <strong>Maryline Pruvost</strong> Transverse Flute, Voice, Stones, <strong>Christian Pruvost</strong> Trumpet.</p>

<p><em>Tombstones</em> is a collection of twenty songs by American composer Michael Pisaro-Liu, associated with the Wandelweiser group, written between 2006 and 2010. These songs navigate between experimental and pop music. In June 2020, Barbara Dang and six other musicians recorded eleven of these pieces at the GMEA – Centre National de Création Musicale in Albi-Tarn, with Maryline Pruvost as the lead singer. The result is a nuanced and ethereal chamber music influenced by pop.</p>

<!-- L'élément suivant est réorganisé avec une meilleure structure -->
<p style="text-align: center; margin-top: 20px;"><strong>Lescence/Gmatique</strong></p>

<a href="https://circum-disc.bandcamp.com/album/lescence-gmatique" style="display: block; text-align: center; margin: 0 auto;">
  <img src="images/CCOV_ado_circle.jpg" alt="Description de l'image" width="500" height="auto" style="display: block; margin: 0 auto;">
</a>

<p style="text-align: center; margin-top: 20px;"><strong>ADOCT</strong></p>

<a href="https://tocmuzzix.bandcamp.com/album/ouvre-glace" style="display: block; text-align: center; margin: 0 auto;">
  <img src="images/COV_adoct_circle.jpg" alt="Description de l'image" width="500" height="auto" style="display: block; margin: 0 auto;">
</a>

<p style="text-align: center; margin-top: 20px;"><strong>Dead Dead Gang</strong></p>

<a href="https://circum-disc.bandcamp.com/album/dead-dead-gang" style="display: block; text-align: center; margin: 0 auto;">
  <img src="images/COV_DDG_circle.jpg" alt="Description de l'image" width="500" height="auto" style="display: block; margin: 0 auto;">
</a>

<p><strong>Maryline Pruvost</strong> Voice, Indian Harmonium, <strong>Barbara Dang</strong> Piano, <strong>Gordon Pym</strong> Electronics, Amplified Objects, <strong>Peter Orins</strong> Drums.</p>

<p><em>Dead Dead Gang</em> is a musical project by drummer Peter Orins, inspired by Alan Moore's novel <em>Jerusalem</em>. This monumental narrative, spanning 2,000 years of history and space, fascinated Orins with its temporal and spatial layers. The project explores this richness by blending open musical writing, minimalism, sacred and popular influences, and great freedom of interpretation. The ethereal and ambiguous work cultivates an obsession with rhythm, the dissolution of silence, and a ghostly atmosphere.</p>

<p style="text-align: center; margin-top: 20px;"><strong>Matt Elliott - Drinking Songs</strong></p>

<a href="https://mattelliott.bandcamp.com/album/drinking-songs-live-20-years-on" style="display: block; text-align: center; margin: 0 auto;">
  <img src="images/COV_drinking_songs.jpg" alt="Description de l'image" width="500" height="auto" style="display: block; margin: 0 auto;">
</a>

<p><strong>Anne-Elisabeth De Cologne</strong> Double Bass, Effects, <strong>Barbara Dang</strong> Piano, Keyboards, Synths, <strong>Matt Elliott</strong> Guitars, Voice, Saxophones, Live sampling, Whistling, Drums</p>

<p>Matt Elliott, born in 1968, is a British musician known for his solo work and under the alias Third Eye Foundation. He blends folk, electronic, and orchestral music, with recurring themes of melancholy, solitude, and the human condition. His albums, such as The Broken Man and Farewell to All We Know, combine raw emotion with subtle arrangements. Elliott stands out for his constant musical evolution, marked by a powerful voice and deeply introspective compositions.</p> 