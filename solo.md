---
title: "solo"
order: 2
in_menu: true
---
<html lang="fr">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Mon Inventaire</title>
  <!-- Lien vers la police Nunito de Google Fonts -->
  <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400&display=swap" rel="stylesheet">
  <style>
    body {
      font-family: 'Nunito', sans-serif; /* Utilisation de la police Nunito */
      line-height: 1.6; /* Améliore l'espacement des lignes */
      margin: 0;
      padding: 20px; /* Ajoute de l'espace autour du contenu */
    }

    h2 {
      font-weight: 400; /* Pour rendre les titres un peu plus légers */
      margin-bottom: 10px; /* Ajoute un peu d'espace sous les titres */
    }

    ul {
      margin-bottom: 20px; /* Ajoute de l'espace après chaque liste */
    }

    li {
      margin-bottom: 5px; /* Ajoute de l'espace entre les éléments de la liste */
    }

    figure {
      text-align: center; /* Centrer l'image et le crédit photo */
    }

    figcaption {
      font-size: 0.8em; /* Taille plus petite pour le crédit photo */
      color: #555; /* Gris foncé pour le crédit */
      margin-top: 5px; /* Espace entre l'image et le crédit */
    }
  </style>
</head>
<body>

  <!-- Ajout de l'image avec le crédit photo -->
  <figure>
    <img src="images/barbara_dang_21_circle.jpg" alt="Description de l'image" width="300">
    <figcaption>Crédit photo : Philippe Lenglet</figcaption>
  </figure>

  <h2>Œuvres de répertoire pour piano</h2>
  <ul>
    <li><i>Dream</i> - John Cage (1948)</li>
    <li><i>Waiting</i> - John Cage</li>
    <li><i>Swinging</i> - John Cage</li>
    <li><i>A room</i> - John Cage (1943)</li>
    <li><i>One5</i> - John Cage (1990)</li>
    <li><i>Unintended piano music</i> - Cornelius Cardew (1950-1960)</li>
    <li><i>Arpa Eolica</i> - Henry Cowell (1923)</li>
    <li><i>Ostinato</i> - Henry Cowell</li>
    <li><i>Clear Edge</i> - Barbara Monk Feldman (1993)</li>
    <li><i>Piano piece 1952</i> - Morton Feldman (1952)</li>
    <li><i>Palai de mari</i> - Morton Feldman (1986)</li>
    <li><i>Sam Lazaro Bros</i> - Jurg Frey (1984)</li>
    <li><i>Spaces</i> - Tom Johnson (1969)</li>
    <li><i>Twelve</i> - Tom Johnson (2008)</li>
    <li><i>Tango</i> - Tom Johnson</li>
    <li><i>Counting keys</i> - Tom Johnson (1982)</li>
    <li><i>Same and Different</i> - Tom Johnson (2004)</li>
    <li><i>Music for Piano With Magnetic Strings</i> - Alvin Lucier (1999)</li>
    <li><i>Musica Callada Cahier 1,2,3,4</i> - Federico Mompou (1951)</li>
    <li><i>Paisajes</i> - Federico Mompou (1942)</li>
    <li><i>Sur la pointe des pieds</i> - Federico Mompou (1916)</li>
    <li><i>Vexations</i> - Erik Satie</li>
    <li><i>Le Fils Des Etoiles</i> - Erik Satie (1891)</li>
    <li><i>Music For John Cage</i> - Linda Catlin Smith (1990)</li>
  </ul>

  <h2>Œuvres de répertoire pour clavecin</h2>
  <ul>
    <li>Ouverture to Orpheus - Louis Andriessen</li>
    <li>Ostinato - Henry Cowell</li>
    <li>Corona - Toru Takemitsu</li>
    <li>Uppon La Mi Ré - Anonyme</li>
    <li>Sleeping Lady - Linda Catlin Smith</li>
    <li>Shadow of Earth - Michael Pisaro-Liu</li>
  </ul>

</body>
</html> 