---
title: "events"
order: 1
in_menu: true
---
<html lang="fr">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Mon Inventaire</title>
  <!-- Lien vers la police Nunito de Google Fonts -->
  <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400&display=swap" rel="stylesheet">
  <style>
    body {
      font-family: 'Nunito', sans-serif; /* Utilisation de la police Nunito */
    }

    p {
      font-family: 'Nunito', sans-serif; /* Si tu veux appliquer Nunito aussi aux paragraphes */
    }
  </style>
</head>
<body>

<p>24.01.2025 | Ingrid Laubrock et le Grand Orchestre de Muzzix | La Malterie, Abbeville</p>
<p>12.12.2024 | Piano Solo | L'Annexe, Bruxelles</p>
<p>25.11.2024 | [LUN19h] Soy Tierra, Duo avec Borja Flames | La Malterie, Lille</p>
<p>17.11.2024 | [Festival Neue Music] Pianoïse | Brème</p>
<p>05.10.2024 | A.R.I.O | Atelier David Boinnard, Ronchin</p>
<p>03.10.2024 | KEZN | Cinéma l'Univers, Lille</p>
<p>25.09.2024 | Flumunda | Le Petit Faucheux, Tours</p>
<p>15.09.2024 | [OUTRE_SONS] Pianoïse | Ancienne Brasserie Bouchoule, Montreuil</p>
<p>27.07.2024 | Pianoïse | Le Bouche à Oreille, Simorre</p>
<p>05.07.2024 | [Anti-Bercy] Piano Solo | Chez l'habitant, Mons-en-Baroeul</p>
<p>07.06.2024 | [A.R.T.S] Radiolarians | Halle B Gare Saint Sauveur, Lille</p>
<p>31.05.2024 | Fluo Sauvage | Singe Savant, Lille</p>
<p>25.05.2024 | Looking for Hart’s Songs | Louvre Lens, Lens</p>
<p>15.04.2024 | [LUN19] Quartet avec X.Charles/E.Normand/M.Pruvost | La Malterie, Lille</p>
<p>07.04.2024 | [Festival Variations] Pianoscape | Lieu Unique, Nantes</p>
<p>06.04.2024 | [Festival Superflux] Pianoïse | Ecole Supérieure d'Art et de Design, Tours</p>
<p>05.04.2024 | [Les Fenêtres qui parlent] Fluo Sauvage | Chez l'habitant, Lille</p>
<p>29.03.2024 | [A.R.T.S] Aragolarians | La Malterie, Lille</p>
<p>16.03.2024 | [Festival Sonic Protest] AGNel/DANg | La Dynamo, Paris</p>
<p>12.02.2024 | Pianoïse | La Méandre, Chalon-sur-Saône</p>
<p>11.02.2024 | Pianoïse | Cave 12, Genève</p>
<p>10.02.2024 | Pianoïse | Why Note, Dijon</p>
<p>09.02.2024 | Pianoïse | Ecole de musique de Woippy, Metz</p>
<p>22.01.2024 | [LUN19H] DEGOM - Aus den Sieben Tagen Stockhausen | La Malterie, Lille</p>
<p>02.01.2023 | [Festival Autres Mesures] Looking For H’arts Songs | Opéra de Rennes</p>
<p>27.11.2023 | [LUN19H] A.R.I.O. | La Malterie, Lille</p>
<p>26.11.2023 | [Happy Days] Vitesse 80 | Opéra de Lille</p>
<p>24.11.2023 | [WIM Festival] avec Aude Romary | Wim, Zurich</p>
<p>23.11.2023 | [Musique&Mathématiques] avec Aude Romary | Université de Besançon</p>
<p>04.11.2023 | invitée dans le TOC Beat Club / La Malterie, Lille
<p>22.10.2023 | Solo d’Orgue | Église de Wandignies-Hamage</p>
<p>07.10.2023 | Abdou/Dang/Orins | Les 26 chaises, Paris</p>
<p>06.10.2023 | [Festival L’homme aux deux oreilles] Abdou/Dang/Orins |, Amiens</p>
<p>05.10.2023 | [Festival Pour les Oiseaux] Abdou/Dang/Orins |, MUMA, Le Havre</p>
<p>02.10.2023 | [M&A] Tombstones II | La Malterie, Lille</p>
<p>24.09.2023 | [M&A] Clavecin Solo | Atelier David Boinnard, Ronchin</p>
<p>17.09.2023 | [M&A] Pianoïse | Gare Saint-Sauveur, Lille<p>
<p>16.09.2023 | [M&A] Piano Solo | Gare Saint-Sauveur, Lille</p>
<p>23.08.2023 | [Bruitkasten #25] Pruvost/Dang/Orins | Gemeinde Köln, Cologne</p>
<p>26.06.2023 | [LUN19H] Piano-Gong avec Fred Loisel | La Malterie, Lille</p>
<p>23.06.2023 | Dead Dead Gang | Annexe 1 - Laboratoire Central, Bruxelles (Be) </p>
<p>20.05.2023 | [Musique Action] Musique de répertoire (piano solo) | CCAM, Vandoeuvre-Les-Nancy</p>
<p>18.05.2023 | [Musique Action] Tom Johnson (piano solo) | CCAM, Vandoeuvre-Les-Nancy</p>
<p>15.05.2023 | Drinking Songs avec Matt Elliott | Café de la Danse, Paris</p>
<p>01.05.2023 | [LUN19] Tom Johnson (piano solo) | La Malterie, Lille</p>
<p>20.04.2023 | Ingrid Laubrock rencontre Muzzix | La Malterie, Lille</p>
<p>27.03.2023 | The Delinquents | La Malterie, Lille</p>
<p>30.01.2023 | Deux ( ), Four Stones de Michael Pisaro-Liu | La Malterie, Lille</p>
<p>12.12.2023 | [LUN19H] Federico Mompou (piano solo) | La Malterie, Lille</p>
<p>02.12.2023 | Fluo Sauvage | La Condition Publique Beau Repaire, Roubaix</p>
<p>04.12.2023 | Fluo Sauvage | Cité des Électriciens, Bruay-la-Buissière</p>
<p>11.12.2023 | Fluo Sauvage | Grand Mix, Tourcoing</p>
<p>06.03.2023 | LUN19h - Barbara Dang solo | La Malterie, Lille</p> 
<p>14.02.2023 | [Atelier d'improvisation à l'ESMD] Tom Johnson | Conservatoire de Lille 
<p>05.12.2022 | [M&A] AGNel/dANG | La Malterie, Lille</p>
<p>22.11.2022 | [M&A] Flumunda | Kursaal, Hellemmes</p>
<p>24.10.2022 | LUN19h - deux ( ) | La Malterie, Lille</p> 