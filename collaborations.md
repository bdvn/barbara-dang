---
title: "collaborations"
order: 5
in_menu: true
---
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Collaborations and Projects</title>
  <!-- Link to Nunito font from Google Fonts -->
  <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400&display=swap" rel="stylesheet">
  <style>
    body {
      font-family: 'Nunito', sans-serif; /* Apply Nunito font */
      line-height: 1.6; /* Improve line spacing */
      margin: 0;
      padding: 20px; /* Add space around the content */
    }

    h2 {
      font-weight: 400; /* Lighter headings */
      margin-bottom: 15px; /* Space below headings */
    }

    ul {
      margin-bottom: 20px; /* Space after each list */
    }

    li {
      margin-bottom: 5px; /* Space between list items */
    }
  </style>
</head>
<body>

<br><h2><strong>Pianoïse</strong></h2>

<img src="{% link images/pianoisetoulouse2.jpg %}" alt="Image description" width="500" height="auto">

<p><strong>Emmanuel Lalande</strong> concept,<strong> Sophie Agnel, Barbara Dang, Félicie Bazelaire, Betty Hovette, Nicolas Lelièvre, Arnaud Le Mindu</strong> pianists</p>

<p>A proposition by Emmanuel Lalande for 6 pianists, 6 pianos, 1,400 strings, 1 tuner, and 1 theater stage. A daring attempt to transform the king of chromatic instruments into a cacophonous machine. For this occasion, the pianos will be tuned in such a way that no string is tuned the same. When all pianos are played together, the sound should resemble white noise or something close to it.</p>

<br><h2><strong>Looking for H'art Songs</strong></h2>

<img src="{% link images/looking_for_harts_songs_louvrelens.jpg %}" alt="Image description" width="500" height="auto">

<p><strong>Barbara</strong> <strong>Dang</strong> upright piano,<strong> Melaine</strong> <strong>Dalibert</strong> upright piano, <strong>Lou</strong> <strong>Renaud-Bailly</strong> trimba, percussion, <strong>Bertrand</strong> <strong>Belin</strong> vocals, <strong>G.W.</strong> <strong>Sok</strong> vocals, <strong>Borja</strong> <strong>Flames</strong> vocals, <strong>John</strong> <strong>Greaves</strong> vocals, <strong>David</strong> <strong>Sztanke</strong> vocals, <strong>Olivier</strong> <strong>Mellano</strong> artistic direction, <strong>Louis</strong> ‘<strong>Moondog</strong>’ <strong>Hardin</strong> composition</p>

<p>Under the artistic direction of Olivier Mellano, <em>Looking For H'Art Songs</em> brings together musicians and voices with varying sensibilities, exploring the work of Louis 'Moondog' Hardin and the vocal pieces collected in the 1978 album <em>H’art Songs</em>.</p>

<br><h2><strong>Flumunda</strong></h2>

<img src="{% link images/flumunda_rdv.jpg %}" alt="Image description" width="500" height="auto">

<p><strong>Christian</strong> <strong>Pruvost</strong> Composition, artistic direction - Trumpet, electronics, <strong>Sakina</strong> <strong>Abdou</strong> Saxophones, flutes, <strong>Sébastien</strong> <strong>Beaumont</strong> Guitar, <strong>Ivann</strong> <strong>Cruz</strong> Guitar, <strong>Barbara</strong> <strong>Dang</strong> Keyboards, <strong>Xuan Mai</strong> <strong>Dang</strong> Vocals, flutes, electronics, <strong>Maryline</strong> <strong>Pruvost</strong> Vocals, flutes, <strong>Peter</strong> <strong>Orins</strong> Drums, electronics, <strong>Alex</strong> <strong>Noclain</strong> Sound projection, <strong>Claire</strong> <strong>Lorthioir</strong> Lighting design</p>


<p>As composer-in-residence at Vivat from 2020-22, trumpet player Christian Pruvost created <em>Flumunda</em>, a piece for eight musicians and tape, inspired by sounds recorded during his explorations of the region. <em>Flumunda</em> evokes the river Lys, its changing nature and many faces. In eight movements, the work blends minimalist and tribal music, microtonality, poetic texts by Thomas Suel, and electroacoustic soundscapes, addressing themes such as pollution and our connection to nature.</p>

<br><h2><strong>Pianoscape</strong></h2>

<img src="{% link images/pianoscape.jpg %}" alt="Image description" width="500" height="auto">

<p><strong>Denis Chouillet</strong>, <strong>Melaine Dalibert</strong>, <strong> Barbara Dang</strong>, <strong>Célimène Daudet</strong> pianos

<p>Four pianist paint four sonic landscapes evocative of vast wild spaces. Harmonic canyons, echoes of plains, and other landscapes dotted with solitary refrains unfold in a concert that features the works of Peter Garland, Harold Budd, Morton Feldman, and Melaine Dalibert.</p>

<br><h2><strong>P.U.N.K</strong></h2>

<img src="{% link images/punk_affiche.jpg %}" alt="Image description" width="500" height="auto">

<p><strong>Renaud Cojo</strong> <strong>Director</strong>, <strong>Annabelle Chambon</strong> <strong>Dancers</strong>, <strong>choreographers</strong>, <strong>Cédric Charron</strong> <strong>Dancers</strong>, <strong>choreographers</strong>, <strong>Antoine Esmérian-Lesimple</strong> <strong>Actor</strong>, <strong>David Chiesa</strong> <strong>Musicians</strong> (electric bass), <strong>Blanche Lafuente</strong> <strong>Musicians</strong> (drums), <strong>Barbara Dang</strong> <strong>Musicians</strong> (keyboard), <strong>Timothée Quost</strong> <strong>Musicians</strong> (trumpet), <strong>Mathieu Werchowski</strong> <strong>Musicians</strong> (violin), <strong>Eric Blosse</strong> <strong>Scenography</strong>, <strong>Florent Blanchon</strong> <strong>Lighting</strong>, <strong>Laurent Rojol</strong> <strong>Video Images</strong>, <strong>Pierre-Olivier Boulant</strong> <strong>Sound and Technical Direction</strong></p>

<p><em>P.U.N.K (People Under No King)</em> is a project combining writing, performance, punk music, and improvisation. It aims to restore a rebellious dimension to punk by merging it with improvisation, two instinctive forces freed from the rules of composition. Created by Renaud Cojo, this project brings together Ensemble Un (David Chiesa) and performers Annabelle Chambon and Cédric Charron, seeking to rediscover the frenzy and anger in the dynamic, raw music of punk.</p>

<br><h2><strong>Four For</strong></h2>

<img src="{% link images/fourfor_gaite_lyrique.jpg %}" alt="Image description" width="500" height="auto">

<p><strong>Halory Goerger</strong> concept, text, scenography, <strong>Antoine Cegarra</strong> performance & artistic collaboration alternating with <strong>Joël Maillard</strong> / <strong>Juliette Chaigneau</strong> / <strong>Barbara Đăng</strong> / <strong>Halory Goerger</strong>, <strong>Germain Wasilewski</strong> general stage direction and construction, <strong>Antoine Villeret</strong> sound and development, <strong>Annie Leuridan</strong> lighting design, <strong>Aurélie Noble</strong> costumes, <strong>Antoine Proux</strong> / <strong>Vincent Combaut</strong> / <strong>Christophe Gregorio</strong> construction.</p>

<p><br>It’s a text about stasis and metastasis.
<br>It’s a piece about the role of music in our lives.
<br>It’s an installation about spaces and hyperspaces.
<br>It’s a story unfolding in the mind of someone who is both very ill and very well at the same time.</p>

<p>1986: American composer Morton Feldman is on his deathbed. A singular therapist, coming to assist in his end-of-life, takes an astral journey inside his unconscious. He gets lost and finds both madness and comfort. Two colleagues try to join him. Together, they struggle with whatever they have at hand and make music in a hostile environment. What happens in a brain that malfunctions, and in which you haven’t been invited?</p>

<br><h2><strong>Fluo Sauvage / Vitesse 80</strong></h2>

<img src="{% link images/2022_Fluo Sauvage_2-3.JPG %}" alt="Image description" width="500" height="auto">

<p><strong>Barbara Dang</strong> keyboards, synthesizer, <strong>Philippe Delgrande</strong> electric guitar, bass guitar, <strong>Frederic L'Homme</strong> electronic drums</p>

<p>80's cover trio, <em>Fluo Sauvage</em> offers up to 4 hours of live performance that sets the dance floor on fire with the greatest hits from the 80s (A-ha, Daniel Balavoine, David Bowie, Étienne Daho, Depeche Mode, George Michael, Marc Lavoine, Madonna, Julie Piétri, Lionel Richie, Technotronic, and many more...</p>


</body>
</html> 