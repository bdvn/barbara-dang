---
title: "home"
order: 0
in_menu: true
---
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Affichage de l'image et texte justifié</title>
    <!-- Lien vers la police Nunito de Google Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400&display=swap" rel="stylesheet">
    <style>
        body {
            font-family: 'Nunito', sans-serif; /* Utilisation de la police Nunito */
        }

        p {
            text-align: justify; /* Justifier le texte */
        }

        figure {
            text-align: center; /* Centrer l'image et le crédit photo */
        }

        figcaption {
            font-size: 0.8em; /* Taille plus petite pour le crédit photo */
            color: #555; /* Gris foncé pour le crédit */
            margin-top: 5px; /* Espace entre l'image et le crédit */
        }
    </style>
</head>
<body>

    <h1>Barbara Dang</h1>

    <figure>
        <img src="images/Barbara1_RDG.png" alt="barbara">
        <figcaption>Credit : Photo by Rémi De Gaalon</figcaption>
    </figure>

    <p>
        A pianist, performer, and improviser from Lille, Barbara Dang is a member of the Muzzix collective. She stands out for the way she connects interpretation and improvisation. Her music is primarily inspired by minimalist repertoire and experimental works, including compositions by several composers such as Linda Catlin Smith, Morton Feldman, Barbara Monk Feldman, Michael Pisaro-Liu, and Federico Mompou. She uses unconventional techniques, such as prepared piano, often engaging with the bridge (or string block) of the instrument. Her approach emphasizes musical action, incorporating silence and environmental sounds, rather than an aestheticized individual expression. Her musical eclecticism leads her to explore other instruments, taking into account their organological characteristics, from the plucked strings of the harpsichord to the breath of the organ. She collaborates with artists such as Sophie Agnel, Borja Flames, Aude Romary, Laure Vovard, Jean-Luc Guionnet, composers Radu Malfatti, Michael Pisaro-Liu, Tom Johnson, theater director Halory Goerger, videographer Lionel Palun, and harpsichord maker David Boinnard. Her participation in various projects ranges from postmodern works with the Ensemble Dedalus to the repertoire Looking for Hart's Songs with Olivier Mellano, as well as Pianoïse by Emmanuel Lalande. Between 2020 and 2021, she released two albums, Lescence/Gmatique and Ouvre-Glace on the Circum-disc and Tour de Bras labels, and recorded Tombstones (a collection of songs by Michael Pisaro) at GMEA – Centre National de Création Musicale Albi — Tarn. Since March 2015, she has been working for Revue & Corrigée, a leading journal for experimental sound practices.
</p>

</body>
</html> 