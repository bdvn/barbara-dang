---
title: "in duo/trio"
order: 3
in_menu: true
---
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Collaborations and Projects</title>
  <!-- Link to Nunito font from Google Fonts -->
  <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400&display=swap" rel="stylesheet">
  <style>
    body {
      font-family: 'Nunito', sans-serif; /* Apply Nunito font */
      line-height: 1.6; /* Improve line spacing */
      margin: 0;
      padding: 20px; /* Add space around the content */
    }

    h2 {
      font-weight: 400; /* Lighter headings */
      margin-bottom: 20px; /* Space below headings */
      margin-top: 30px; /* Space above each heading */
    }

    ul {
      margin-bottom: 20px; /* Space after each list */
    }

    li {
      margin-bottom: 5px; /* Space between list items */
    }

    figure {
      text-align: left; /* Align images to the left */
      margin: 0;
      padding: 0;
    }

    figcaption {
      font-size: 0.8em;
      color: #555;
      margin-top: 5px;
      display: block;
      text-align: left; /* Align image credit to the left */
    }

    img {
      display: block;
      margin: 0; /* Remove automatic margin */
      width: 350px; /* Set image width */
      height: auto;
    }

    p {
      text-align: justify; /* Justify the text */
    }
  </style>
</head>
<body>

  <h2><strong>Soy Tierra</strong></h2>
  <figure>
    <img src="images/BORJABARBARA.jpg" alt="Borja Flames and Barbara Dang playing music">
    <figcaption>Credit: Photo by Rémi De Gaalon</figcaption>
  </figure>
  <p><strong>Borja Flames</strong>, voice, tar, objects, <strong>Barbara Dang</strong>, piano</p>
  <p>The composer Borja Flames and pianist Barbara Dang met during a tribute to Moondog...</p>

  <h2><strong>Tombée for harpsichord solo</strong></h2>
  <figure>
    <img src="images/schema_clavecin.jpg" alt="Performance on harpsichord">
  </figure>
  <p><strong>Jean-Luc Guionnet</strong> composition, <strong>Barbara Dang</strong> performance</p>
  <p><em>Tombée</em> is a work for solo harpsichord, dedicated to Barbara Dang and composed by Jean-Luc Guionnet...</p>

  <h2><strong>ARIO</strong></h2>
  <figure>
    <img src="images/ARIO_chez_DavidB.jpg" alt="Barbara Dang with David Boinnard">
  </figure>
  <p><strong>Barbara Dang</strong>, harpsichord, <strong>David Boinnard</strong>, instrument maker, mechanized virginal, video, voice, typewriter...</p>
  <p><em>ARIO for hARpsIchOrd</em>: neither a concert nor a conference, but a performance where the harpsichord reveals itself...</p>

  <h2><strong>Dang/Romary</strong></h2>
  <figure>
    <img src="images/romary_dang.jpg" alt="Aude Romary and Barbara Dang">
  </figure>
  <p><strong>Barbara Dang</strong>, piano, <strong>Aude Romary</strong>, cello</p>
  <p>Aude Romary and Barbara Dang take an inventive and experimental approach to music...</p>

  <h2><strong>Duo AGNel/DANg</strong></h2>
  <figure>
    <img src="images/agnel_dang.jpg" alt="Sophie Agnel and Barbara Dang in a piano duet">
    <figcaption>Credit: Photo by HDOK-Hervé Goluza</figcaption>
  </figure>
  <p><strong>Sophie Agnel</strong>, <strong>Barbara Dang</strong>, four-hand piano</p>
  <p>Sophie Agnel and Barbara Dang explore improvised music with a personal use of extended piano techniques...</p>

  <h2><strong>Abdou/Dang/Orins</strong></h2>
  <figure>
    <img src="images/abdou-dang-orins_22_c_philippe_lenglet-3-3ce06.jpg" alt="Sakina Abdou, Barbara Dang, and Peter Orins performing">
    <figcaption>Credit: Photo by Philippe Lenglet</figcaption>
  </figure>
  <p><strong>Sakina Abdou</strong>, saxophones, flute, <strong>Barbara Dang</strong>, piano, <strong>Peter Orins</strong>, drums</p>
  <p>A trio consisting of saxophone, piano, and drums that creates notes, noise, and perhaps melodies...</p>

  <h2><strong>Deux( )</strong></h2>
  <figure>
    <img src="images/deux().jpg" alt="Gordon Pym and Barbara Dang performing">
  </figure>
  <p><strong>Gordon Pym</strong>, objects, electronics, <strong>Barbara Dang</strong>, piano, objects, electronics</p>
  <p><em>deux( )</em> is a project dedicated to performing experimental pieces...</p>

  <h2><strong>KEZN</strong></h2>
  <figure>
    <img src="images/kezn_ckezn_web.jpg" alt="Performance of the Kezn project">
    <figcaption>Credit: Photo by Lionel Palun</figcaption>
  </figure>
  <p>The project <em>Kezn</em> reinterprets remarkable processes from the experimental music repertoire...</p>

  <h2><strong>Jacqueline</strong></h2>
  <figure>
    <img src="images/jacqueline_aliette.jpg" alt="Barbara Dang, Mimosa, and Anne Sortino performing">
    <figcaption>Credit: Draw by Aliette</figcaption>
  </figure>
  <p><strong>Barbara Dang</strong>, piano, <strong>Mimosa</strong>, guitar, lap steel, <strong>Anne Sortino</strong>, violin</p>
  <p>A musical cocktail with white wine, grenadine, and lemonade mixed...</p>

  <h2><strong>DAN/GO</strong></h2>
  <figure>
    <img src="images/Dango.jpg" alt="Barbara Dang and Raphaël Godeau in a duet">
  </figure>
  <p><strong>Barbara Dang</strong>, piano, virginal, <strong>Raphaël Godeau</strong>, guitar, mandolin</p>
  <p><em>DAN/GO</em> is a piano-guitar (or virginal-mandolin) duo. The music is improvised...</p>

</body>
</html> 